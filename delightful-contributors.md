# delightful contributors

These fine people brought us delight by adding their gems of freedom to the delight project.

> **Note**: This page is maintained by you, contributors. Please create a pull request or issue in our tracker if you contributed list entries and want to be included. [More info here](https://codeberg.org/teaserbot-labs/delightful/src/branch/main/delight-us.md#attribution-of-contributors).

## We thank you for your gems of freedom :gem:

- [Arnold Schrijver](https://community.humanetech.com/u/aschrijver/summary) (codeberg: [@circlebuilder](https://codeberg.org/circlebuilder), fediverse: [@humanetech@mastodon.social](https://mastodon.social/@humanetech))
- Danyl Strype (codeberg: @strypey, fediverse: [@strypey@mastodon.nzoss.nz](https://mastodon.nzoss.nz/@strypey))
- [Matthew Evan](https://matthewevan.xyz) (codeberg: [@MattMadness](https://codeberg.org/MattMadness), fediverse: [@mattmadness@mastodon.online](https://mastodon.online/@mattmadness))
- [Gaspard Wierzbinski](https://cpluspatch.com) (codeberg: [@CPlusPatch](https://codeberg.org/CPlusPach), fediverse: [@cpluspatch@fedi.cpluspatch.com](https://fedi.cpluspatch.com/cpluspatch), matrix: [@jesse:cpluspatch.dev](https://matrix.to/#/@jesse:cpluspatch.dev))
- [Liaizon Wakest](https://wake.st) (codeberg: [@wakest](https://codeberg.org/wakest), fediverse: [@liaizon@wake.st](https://social.wake.st/@liaizon), matrix: [@wakest:polyphaseportal.xyz](https://matrix.to/#/@wakest:polyphaseportal.xyz)